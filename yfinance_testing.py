#!/usr/bin/env python3

"""
Sources:
https://pypi.org/project/yfinance/
chatgpt
stackoverflow
https://medium.com/@s.sadathosseini/real-time-apple-stock-prices-visualization-using-yfinance-and-streamlit-c4466d0a9b51
https://docs.python.org/3/library/sqlite3.html
"""

import yfinance as yf
import sqlite3
import time

# db section
## Create db
print(sqlite3.version)
print(sqlite3.sqlite_version)

# Create and connect to a new or existing database
db_conn = sqlite3.connect('test_data2.db')
# Define db cursor to allow execute and fetching from the SQL db
db_curser = db_conn.cursor()

# Using the cursor the db table named "test_data2" can be created. Just below the columns are being defined with the type of data they will include.
db_curser.execute('''CREATE TABLE IF NOT EXISTS test_data2
            (Date TEXT, Open REAL, High REAL, Low REAL, Close REAL, Adj_Close REAL, Volume INTEGER)''')

# Retrieve stock data using yfinance and insert it into the SQLite table
def save_stock_data_to_db(symbol):
    # Retrieve data from Yahoo Finance
    data = yf.download(symbol)
    # print(data)

    # Insert data into the SQLite table using curser again and "INSERT_INTO"
    for index, row in data.iterrows():
        # print(index)
        # print(row)
        # ? used to avoid sql injection attacks
        db_curser.execute("INSERT INTO test_data2 VALUES (?, ?, ?, ?, ?, ?, ?)", 
                  (str(index), row['Open'], row['High'], row['Low'], row['Close'], row['Adj Close'], row['Volume']))
    
    # Commit and close
    db_conn.commit()
    db_conn.close()

# Example usage:
save_stock_data_to_db('MSFT')  # Replace 'AAPL' with the desired stock symbol


# Read the database
def read_stock_data_from_db():
    db_conn = sqlite3.connect('test_data2.db')  # Connect to the SQLite database
    db_curser = db_conn.cursor()

    # Execute a SELECT query to retrieve all rows from the stock_data table
    db_curser.execute("SELECT * FROM test_data2")

    # Fetch all rows returned by the query
    rows = db_curser.fetchall()

    # Print the fetched rows
    for row in rows[-10:]:
        print(row)

    # Close the connection
    db_conn.close()

# Example usage:
read_stock_data_from_db()

# Stackoverflow
tickers = ['GTLB']
for ticker in tickers:
    ticker_yahoo = yf.Ticker(ticker)
    data = ticker_yahoo.history()
    #print(data)
    last_quote = data['Close'].iloc[-1]
    #print(ticker, last_quote)

# medium.com
# Define the ticker symbol for Apple
ticker_symbol = 'AAPL'

# Get the data of the stock
apple_stock = yf.Ticker(ticker_symbol)

# Get the historical prices for Apple stock
historical_prices = apple_stock.history(period='1d', interval='1m')

# Get the latest price and time
latest_price = historical_prices['Close'].iloc[-1]
latest_time = historical_prices.index[-1].strftime('%H:%M:%S')

print(ticker_symbol, latest_time, latest_price)


# yfinance section
# symbols = yf.Tickers('msft aapl goog')

# access each ticker using (example)
# print(symbols.tickers['MSFT'].info)
# print(symbols.tickers['AAPL'].history(period="1d"))
# print(symbols.tickers['GOOG'].actions)

# # get historical market data
# hist = msft.history(period="1mo")
# # print(hist)

# # show meta information about the history (requires history() to be called first)
# msft_hist_metadata = msft.history_metadata
# # print(msft_hist_metadata)

# # show actions (dividends, splits, capital gains)
# actions = msft.actions
# dividens = msft.dividends
# splits = msft.splits
# capital = msft.capital_gains  # only for mutual funds & etfs
# # print(capital)

# # show share count
# shares = msft.get_shares_full(start="2022-01-01", end=None)
# # print(shares)

# # show financials:
# # - income statement
# msft.income_stmt
# msft.quarterly_income_stmt
# # - balance sheet
# msft.balance_sheet
# msft.quarterly_balance_sheet
# # - cash flow statement
# msft.cashflow
# msft.quarterly_cashflow
# # see `Ticker.get_income_stmt()` for more options
# # print(msft.quarterly_cashflow)

# # show holders
# msft.major_holders
# msft.institutional_holders
# msft.mutualfund_holders
# msft.insider_transactions
# msft.insider_purchases
# msft.insider_roster_holders
# # print(msft.insider_purchases)

# # show recommendations
# msft.recommendations
# msft.recommendations_summary
# msft.upgrades_downgrades
# print(msft.recommendations_summary)
# # print(msft.recommendations)

# # Show future and historic earnings dates, returns at most next 4 quarters and last 8 quarters by default. 
# # Note: If more are needed use msft.get_earnings_dates(limit=XX) with increased limit argument.
# msft.earnings_dates

# # show ISIN code - *experimental*
# # ISIN = International Securities Identification Number
# msft.isin

# # show options expirations
# msft.options
# # print(msft.options)

# # show news
# msft.news

# # get option chain for specific expiration
# opt = msft.option_chain('2025-06-20')
# # print(opt)
# # data available via: opt.calls, opt.puts